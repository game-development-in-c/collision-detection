FROM    debian:bookworm-slim
LABEL   author="Aleksandar Atanasov aleksandar.vl.atanasov@gmail.com"

RUN     apt-get update && apt-get -y install \
        gfortran cmake gcc g++

